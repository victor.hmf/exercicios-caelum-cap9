package View;

import Model.Conta;
import Model.ContaCorrente;
import Model.ContaPoupanca;

public class TestaContas 

{
	public static void main(String[] args) 
	{
	
		ContaCorrente contaCorrente1 = new ContaCorrente();
		ContaPoupanca contaPoupanca1 = new ContaPoupanca();
	
		contaCorrente1.deposita(1500);
		contaPoupanca1.deposita(1500);
		
		contaCorrente1.atualiza(0.01);
		contaPoupanca1.atualiza(0.01);

	
		System.out.println(contaCorrente1.getSaldo());
		System.out.println(contaPoupanca1.getSaldo());

	}
}